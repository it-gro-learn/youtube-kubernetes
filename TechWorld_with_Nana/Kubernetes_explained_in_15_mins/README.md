## What is Kubernetes | Kubernetes explained in 15 mins
* https://www.youtube.com/watch?v=VnvRFRk_51k
* [screenshots](screenshots)

```
00:00 - Intro
00:59 - Official Definition
01:40 - What problems does Kubernetes solve?
02:35 - What features do container orchestration tools offer?
03:40 - Basic architecture: Master-Slave nodes, Kubernetes processes
08:08 - Basic concepts: Pods, Containers, Services. What is the role of each?
11:31 - Example Configuration File
```
