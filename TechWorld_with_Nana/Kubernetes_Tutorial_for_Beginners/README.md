## Kubernetes Tutorial for Beginners [FULL COURSE in 4 Hours]
* https://www.youtube.com/watch?v=X48VuDVv0do


```
0:00:00 - Intro and Course Overview
0:02:18 - What is K8s
0:05:20 - Main K8s Components
0:22:29 - K8s Architecture
0:34:47 - Minikube and kubectl - Local Setup
0:44:52 - Main Kubectl Commands - K8s CLI
1:02:03 - K8s YAML Configuration File
1:16:16 - Demo Project: MongoDB and MongoExpress
1:46:16 - Organizing your components with K8s Namespaces
2:01:52 - K8s Ingress explained
2:24:17 - Helm - Package Manager
2:38:07 - Persisting Data in K8s with Volumes
2:58:38 - Deploying Stateful Apps with StatefulSet
3:13:43 - K8s Services explained
```

```
What is Kubernetes
* What problems does Kubernetes solve?
* What features do container orchestration tools offer?

Main K8s Components 
* Node & Pod
* Service & Ingress
* ConfigMap & Secret
* Volumes
* Deployment & StatefulSet

K8s Architecture 
* Worker Nodes
* Master Nodes
* Api Server
* Scheduler
* Controller Manager
* etcd - the cluster brain

Minikube and kubectl - Local Setup 
* What is minikube?
* What is kubectl?
* install minikube and kubectl
* create and start a minikube cluster
```

* https://minikube.sigs.k8s.io/docs/start/
* https://kubernetes.io/docs/tasks/tools/install-kubectl/
* https://github.com/ahmetb/kubectx
* https://gitlab.com/nanuchi/youtube-tutorial-series/-/blob/master/basic-kubectl-commands/cli-commands.md


```
git clone https://gitlab.com/nanuchi/youtube-tutorial-series.git
```

