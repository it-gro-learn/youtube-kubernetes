# TechWorld with Nana
* https://www.techworld-with-nana.com
* https://dev.to/techworld_with_nana
* https://www.instagram.com/techworld_with_nana
* https://gitlab.com/nanuchi
* https://www.youtube.com/c/TechWorldwithNana
* Playlist DevOps Tools: https://www.youtube.com/playlist?list=PLy7NrYWoggjxKDRWLqkd4Kbt84XEerHhB
* Playlist Kubernetes Demo: https://www.youtube.com/playlist?list=PLy7NrYWoggjy3urR5g7BLJiNjLtQcVckT

## Kubernetes
* https://www.youtube.com/c/TechWorldwithNana/search?query=kubernetes
* [Kubernetes_explained_in_15_mins](Kubernetes_explained_in_15_mins)
